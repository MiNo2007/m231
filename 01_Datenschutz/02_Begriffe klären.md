# Wichtigste Begriffe klären
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit (3 bis 4 Personen) |
| Aufgabenstellung  | Erfahrungsaustausch |
| Zeitbudget  |  1 bis 2 Lektionen |
| Ziel | Wichtigste Fragen gemeinsam Diskutieren, Wichtigste Punkte auf Poster festhalten |

## Aufgabenstellung
Setzen Sie sich in der Gruppe zusammen, diskutieren und recherchieren Sie gemeinsam zum Thema Datenschutzgesetz. Notieren Sie die wichtigsten Erkenntnisse auf einem Poster (Sie dürfen ihre Erkenntnisse auch grafisch festhalten). Anschliessend präsentiert jemand der Gruppe das Poster. Nutzen Sie als Grundlage / Inspiration die nachfolgenden Fragen: 

Jede Gruppe diskutiert folgende Fragen und hält die wichtigsten Punkte stichwortartig auf dem Poster fest. 
 - Ist Datenschutz wichtig?
 - Weshalb ist Datenschutz für mich wichtig?
 - Vor was schützt Sie das Datenschutzgesetz?
 
Jede Gruppe erhält von der Lehrperson eine Nummer zugeteilt. Beantworten Sie nur die Frage mit Ihrer Nummer.

### Variante 1
 1. Das eigene Bild: Alles was recht ist. - Die wichtigsten Punkte
 2. Pornografie: Alles was recht ist. - Die wichtigsten Punkte
 3. Cybermobbing: Alles was Recht ist. - Die wichtigsten Punkte
 4. Datenschutz - Schutz der Personendaten - Was sagt die Menschenrechtskonvention dazu?
 5. Rechtfertigungsgrund - Was ist das? In welchen Themen brauchen wir Rechtfertigungsgründe? Darf ein Online-Shop nach der Blutgruppe fragen? Weshalb nicht?


### Variante 2
 
 1. Was ist ein Meme? Welche Memes sind erlaubt, welche nicht? Gibt es Regeln?
 2. Wo finde ich das Datenschutzgesetz? Von wann ist die aktuelle Fassung? Was steht da so drin?
 3. Welche Länder haben einen gleichwertiges Datenschutzgesetz? Was sind meine Rechte (Stichwort Auskunftsrecht)?
 4. Was ist Cybermobbing? Hat Cybermobbing etwas mit Datenschutz zu tun? Begründen Sie. 
 5. Was ist sicherer: Telegram, Signal, Whats-App? Weshalb?
 6. Was ist Identitätsdiebstahl? Was sind die Folgen?
