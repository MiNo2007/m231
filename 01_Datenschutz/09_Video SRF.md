
# Video zum Thema Datenschutz (SRF)
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit |
| Aufgabenstellung  | Video anschauen und kritisch hinterfragen |
| Zeitbudget  |  1 Lektion |
| Ziel | Sie setzen sich kritisch mit einem Video zum Thema Datenschutz auseinander und analysieren welche Dinge immer noch gelten und welche sich verändert haben. |

## 4.1. Aufgabenstellung
Schauen Sie sich den [14-minütigen Film zum Thema Datenschutz](https://www.srf.ch/sendungen/myschool/datenschutz-2) aus dem Jahre 2013 an. 

Diskutieren Sie mit Ihren Mitlernenden folgende Fragen:
 - Was trifft heute immer noch zu?
 - Was trifft heute nicht mehr zu (Was hat sich verändert)?