# M231 - Datenschutz und Datensicherheit anwenden
cd existing_repogit remote add origin https://gitlab.com/m2319663036/m231.git
git branch 
-M maingit push 
-uf origin main

Setzt Datenschutz und Datensicherheit bei Informatiksystemen ein. Überprüft vorhandene Systeme auf Einhaltung von Richtlinien.

Eine Übersicht über die im Modul 231 zu erreichenden Kompetenzen finden Sie im Abschnitt [Kompetenzmatrix](00_kompetenzband/)

# Modulinhalt
 - [Datenschutz](01_Datenschutz/)
 - [Verschlüsselung](03_Passw%C3%B6rter/)
 - [Passwortverwaltung](03_Passw%C3%B6rter/)
 - [Ablagesysteme](04_Ablagesysteme/)
 - [Backup](05_Backup/)
 - [BackupProbleme-und-Juristisches](06_BackupProbleme-und-Juristisches/)
 - [Lizenzmodelle](07_Lizenzmodelle/)
 - [Git / Markdown](10_Git/)

# Leistungsbeurteilung

Informationen dazu finden Sie unter [99_Leistungsbeurteilung](/99_Leistungsbeurteilung/). 
 
 # Unterlagen
 - Gitrepository [ch-tbz-it/Stud/m231](https://gitlab.com/ch-tbz-it/Stud/m231)
 - Gitbook [ch-tbz-it.gitlab.io/Stud/m231/](https://ch-tbz-it.gitlab.io/Stud/m231/)
 - Unterlagen die aus urheberrechtlichen Gründen
<br>nicht hier veröffentlicht werden können, 
<br>teilt Ihnen die Lehrperson mit, wo sie die
<br>finden können (z.B. Sharepoint, Teams, ...) 

# Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
